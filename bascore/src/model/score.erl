%%%%%%%%%%%%%%%%%%%%%%
% Este é o modelo da aplicação. 
%
% No framework ChicagoBoss, representamos modelos como módulos parametrizados. 
% Cada parâmetro do módulo é um atributo do modelo. Podemos colocar tipos nos 
% atributos, mas para este exemplo não é necessário.  
%
% Também é possível colocar métodos de callback parecidos com os de Rails.
%
% Veja a documentação do projeto!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-module(score, [Id, TeamAName, TeamBName, TeamAScore, TeamBScore]).
-compile(export_all).

after_update() ->
	boss_mq:push(Id ++ "-watch", THIS).