-module(bascore_score_controller, [Req, SessionId]). % Módulo parametrizado com a Requisição!
-compile(export_all). % Exporta todas as funções do módulo

-define(QUEUE_SUFFIX, "-watch").
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Processa requisições GET e POST para o path 'score/create'
%
% Quando a action retorna 'ok', significa que a resposta está em um template 
% com o nome da action dentro de 'src/view' mais o nome do meio do controller. 
% No caso: 'src/view/score/create.html'
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
create('GET', []) ->
    ok; % simplesmente renderiza o template sem parâmetros
create('POST', []) ->
    % Pega os parâmetros da requisição
    TeamAName = Req:post_param("team_a_name"),
    TeamBName = Req:post_param("team_b_name"),

    % Cria um novo score 
    DummyScore = score:new(id, TeamAName, TeamBName, 0, 0),

    % Salva o score
    {ok, Score} = DummyScore:save(),

    boss_mq:push(Score:id() ++ ?QUEUE_SUFFIX, Score),

    % Redireciona para ação admin com o id do placar
    {redirect, "admin/"++Score:id()}.   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Processa requisições GET e POST para '/score/admin/' mais o id do placar
%
% Quando a action retorna {ok, [{param1, Value1}, ...,{paramN, ValueN}]}, também se procura 
% um template com as mesmas regras do create(), porém se passa algumas variáveis que podem 
%ser utilizadas com a sintaxe do Django como {{ variavel.atr }}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
admin('GET', [ScoreId])->

    % Pega o placar no banco
    Score = boss_db:find(ScoreId),

    % Renderiza o template com a variável score para ser utilizada
    {ok, [{score, Score}]};

admin('POST', [ScoreId]) ->

    % Pega o placar do banco
    Score = boss_db:find(ScoreId),
    
    % Pega os parâmetros da requisição
    AScore = list_to_integer(Req:post_param("team_a_score")),
    BScore = list_to_integer(Req:post_param("team_b_score")),

    % Atualiza o placar
    UpdatedScore = Score:set([{team_a_score, AScore}, {team_b_score, BScore}]), 

    % Salva. Aqui, um evento será gerado para o listener!!!
    UpdatedScore:save(),

    % Renderiza o template com a variável score para ser utilizada
    {ok, [{score, UpdatedScore}]}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Processa requisições GET para a url '/score/pull/' mais o timestamp e o id do placar
%
% Esta requisição é feita por JavaScript no cliente. 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
pull('GET', [LastTimestamp, ScoreId]) ->

    % Prepara o timestamp
    Ts = list_to_integer(LastTimestamp),

    % Esta chamada bloqueia até haver um evento na fila. 
    % Por isso chamamos essa técnica de long polling
    {ok, Timestamp, [Score]} = boss_mq:pull(ScoreId ++ ?QUEUE_SUFFIX, Ts),

    % Retorna os parâmetros em um objeto JSON.    
    {json, [{timestamp, Timestamp}, {score, Score}]}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Processa requisições GET para a url '/score/live' mais o id do placar
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
live('GET', [ScoreId]) ->

    Ts = boss_mq:now(ScoreId ++ ?QUEUE_SUFFIX),

    % Renderiza o template com as variáveis score e timestamp 
    {ok, [{score, boss_db:find(ScoreId)}, {timestamp, Ts}]}.